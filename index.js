const { watchToken, watchSharding, redeemTokens, minGuildSize, debugShard } = require('./config')
const superagent = require('superagent')
const { format } = require('date-fns')
const { Gateway } = require('detritus-client-socket')

const giftPattern = /\bdiscord\.gift\/(?:[a-z0-9]{16}|[a-z0-9]{24})\b/gi
const instanceCount = +process.env.instances
const instanceId = +process.env.NODE_APP_INSTANCE
const socketsMap = {}
const guildSizeMap = {}
const shardGuildTimeoutMap = {}
const shardGuildCountMap = {}
const attemptedGifts = {}
const getTimeFmt = () => format(new Date(), 'MM/dd hh:mm:ss')

const onPacket = shardId => async function onPacket (packet) {
  switch (packet.t) {
    case 'GUILD_CREATE':
      if (packet.d.member_count >= minGuildSize) {
        guildSizeMap[packet.d.id] = packet.d.member_count
        shardGuildCountMap[shardId]++
      }
      if (shardGuildTimeoutMap[shardId] == null) return
      if (shardGuildTimeoutMap[shardId]) clearTimeout(shardGuildTimeoutMap[shardId])
      shardGuildTimeoutMap[shardId] = setTimeout(() => {
        delete shardGuildTimeoutMap[shardId]
        socketsMap[shardId].emit('sniperReady')
      }, 5000)
    break
    case 'MESSAGE_CREATE':
      if (packet.d.guild_id in guildSizeMap) {
        const gifts = packet.d.content.match(giftPattern)
        if (!gifts) return
        for (const gift of gifts) {
          const giftId = gift.slice(13)
          if (attemptedGifts[giftId]) continue
          attemptedGifts[giftId] = true
          const random = Math.random()
          const redeemToken = Object.keys(redeemTokens).find(token => random < redeemTokens[token])
          superagent.post(`https://discordapp.com/api/v6/entitlements/gift-codes/${giftId}/redeem`)
          .set('Authorization', redeemToken)
          .end((error, response) => {
            if (error) {
              if (response) console.log(`\x1b[31m[${getTimeFmt()}] ${error.status} ${error.message} | ${giftId}\x1b[39m ${response.text}`)
              else console.error(error)
            } else {
              console.log(`\x1b[32m[${getTimeFmt()}] 200 OK | ${giftId}\x1b[39m ${response.text}`)
            }
          })
        }
      }
    break
  }
}

async function spawnSockets () {
  let lastSpawn = Date.now()
  let c = 1
  if (watchSharding) {
    c = watchSharding.spawnCount
    if (instanceCount > 1) c /= instanceCount
  }
  const firstShard = c * instanceId
  const lastShard = c * instanceId + c - 1
  for (let i = firstShard; i <= lastShard; i++) {
    if (i) await new Promise(resolve => setTimeout(resolve, Math.min(5000 - (Date.now() - lastSpawn), 0)))
    let shardId
    if (i === 0 && debugShard != null) shardId = debugShard
    else if (i === debugShard) shardId = 0
    else shardId = i
    const socketOptions = { compress: true }
    if (watchSharding) {
      Object.assign(socketOptions, {
        shardCount: watchSharding.max,
        shardId: shardId
      })
    }
    const socket = new Gateway.Socket(watchToken, socketOptions)
    shardGuildTimeoutMap[shardId] = true
    shardGuildCountMap[shardId] = 0
    socketsMap[shardId] = socket
    socket.on('packet', onPacket(shardId))
    socket.connect('wss://gateway.discord.gg/')
    if (watchSharding) console.log(`\x1b[34m[${getTimeFmt()}] |\x1b[39m Connecting to shard \x1b[36m${shardId}\x1b[39m...`)
    else console.log(`\x1b[34m[${getTimeFmt()}] |\x1b[39m Connecting`)
    socket.once('ready', () => {
      lastSpawn = Date.now()
    })
    await new Promise(resolve => {
      socket.once('sniperReady', () => {
        console.log(`\x1b[34m[${getTimeFmt()}] |\x1b[39m Connected${
          watchSharding
          ? ` to shard \x1b[36m${shardId} (${i - firstShard + 1}/${c})\x1b[39m`
          : ''
        }, listening to \x1b[36m${shardGuildCountMap[shardId]}\x1b[39m guilds`)
        if (i === lastShard && watchSharding) {
          console.log(`\x1b[34m[${getTimeFmt()}] |\x1b[39m All \x1b[36m${lastShard - firstShard}\x1b[39m shards have connected,`
            + ` listening to a total of \x1b[36m${Object.keys(guildSizeMap).length}\x1b[39m guilds`)
        }
        resolve()
      })
    })
  }
}
spawnSockets()
