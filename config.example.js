module.exports = {
  watchToken: 'token', // Token for incoming messages
  watchSharding: { // Sharding options (optional)
    max: 128, // Max amount of
    spawnCount: 16 // Amount of shards to spawn (will start from 0)
  },
  redeemTokens: {
    'token': (1 / 3),
    'token': (2 / 3),
    'token': (3 / 3),
  }, // Token chances for redeeming gifts
  minGuildSize: 1000, // Minimum guild size to snipe
  debugShard: 0 // Override the first shard (optional)
}
